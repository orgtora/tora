# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import json


class task:
    _id = -1
    _result = None
    _test = None
    _name = None
    _dependent_task_array = []
    _is_disabled = False
    _command = None
    _working_directory = None
    _will_fail = False
    _env = {}

    def __init__(self, task_id, test):
        self._id = task_id
        self._test = test
        self._dependent_task_array = [deptest.id() for deptest in test.dependent_test_array()]
        self._is_disabled = test.is_disabled()
        self._command = test.command()
        self._working_directory = test.working_directory()
        self._will_fail = test.will_fail()
        self._name = test.name()
        self._env = test.env()

    def dependent_task_array(self):
        return self._dependent_task_array

    def command(self):
        return self._command

    def name(self):
        return self._name

    def env(self):
        return self._env

    def id(self):
        return self._id

    def is_disabled(self):
        return self._is_disabled

    def result(self):
        return self._result

    def working_directory(self):
        return self._working_directory

    def set_result(self, result):
        self._result = result

    def will_fail(self):
        return self._will_fail

    def test(self):
        return self._test

    def __str__(self):
        return json.dumps(self.toJSON(), indent=2)

    def __repr__(self):
        return json.dumps(self.toJSON(), indent=2)

    def toJSON(self):
        value = {}
        value["command"] = self._command
        value["id"] = self._id
        value["name"] = self._name
        value["dependent_task_array"] = self._dependent_task_array
        value["working_directory"] = self._working_directory
        value["will_fail"] = self._will_fail
        value["is_disabled"] = self._is_disabled
        value["env"] = self._env
        return value


class result:
    _id = -1
    _name = None
    _return_code = 0
    _signal = 0
    _pass = False
    _time = 0
    _stdout = ""

    def __init__(self, task, return_code, signal, time, stdout):
        self._id = task.id()
        self._name = task.name()
        self._return_code = return_code
        self._signal = signal
        self._time = time
        self._stdout = stdout

        if task.will_fail():
            if return_code == 0 and signal == 0:
                self._pass = False
            else:
                self._pass = True
        else:
            if return_code == 0 and signal == 0:
                self._pass = True
            else:
                self._pass = False

    def id(self):
        return self._id

    def name(self):
        return self._name

    def time(self):
        return self._time

    def return_code(self):
        return self._return_code

    def signal(self):
        return self._signal

    def has_passed(self):
        return self._pass

    def __str__(self):
        return json.dumps(self.json(), indent=2)

    def __repr__(self):
        return json.dumps(self.json(), indent=2)

    def json(self):
        value = {}
        value["id"] = self._id
        value["name"] = self._name
        value["signal"] = self._signal
        value["return_code"] = self._return_code
        value["pass"] = (lambda a: "PASS" if a else "FAIL")(self._pass)
        value["stdout"] = self._stdout.decode()
        return value


def from_json(json_data):
    t = task.__new__(task)
    t._id = json_data["id"]
    t._name = json_data["name"]
    t._command = json_data["command"]
    t._dependent_task_array = json_data["dependent_task_array"]
    t._working_directory = json_data["working_directory"]
    t._will_fail = json_data["will_fail"]
    t._env = json_data["env"]
    t._is_disabled = json_data["is_disabled"]
    return t


def parse_task_file(task_file_path):
    task_array = []
    with open(task_file_path, "r") as task_file:
        task_array_json = json.load(task_file)

        for task_json in task_array_json:
            task_array.append(from_json(task_json))
    return task_array


def create_task_array(test_array):
    task_array = []
    for test in test_array:
        task_array.append(task(test.id(), test))
    return task_array
