# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------

import re


def get_dictionary_type(dictionary):
    key_list = []
    for key in dictionary:
        key_list.append(key)
    return key_list


def get_list_of_dict_type(list_of_dict):
    key_list = []
    first_dictionary = list_of_dict[0]
    for key in first_dictionary:
        key_list.append(key)

    if not is_homogenous_list_of_dict(list_of_dict):
        raise ValueError("List of Dict is not Homogenous")

    return key_list


def is_homogenous_list_of_dict(list_of_dict):
    key_list = []
    first_dictionary = list_of_dict[0]
    for key in first_dictionary:
        key_list.append(key)

    for dictionary in list_of_dict:
        for key in dictionary:
            if not key in key_list:
                return False

    return True


def is_type_equal_dictionary(dictionary_a, dictionary_b):
    type_a = get_dictionary_type(dictionary_a)
    type_b = get_dictionary_type(dictionary_b)
    return type_a == type_b


def is_type_conflict_dictionary(dictionary_a, dictionary_b):
    type_a = get_dictionary_type(dictionary_a)
    type_b = get_dictionary_type(dictionary_b)
    for type_value_a in type_a:
        if type_value_a in type_b:
            return True

    return False


def is_type_equal_list_of_dict(list_of_dict_a, list_of_dict_b):
    type_a = get_dictionary_type(list_of_dict_a)
    type_b = get_dictionary_type(list_of_dict_b)
    return type_a == type_b


def is_type_conflict_list_of_dict(list_of_dict_a, list_of_dict_b):
    if not list_of_dict_a:
        return False
    elif not list_of_dict_b:
        return False
    else:
        type_a = get_list_of_dict_type(list_of_dict_a)
        type_b = get_list_of_dict_type(list_of_dict_b)
        for type_value_a in type_a:
            if type_value_a in type_b:
                return True

        return False


def is_type_conflict_list_of_dict_value(list_of_dict_a, key):
    if not list_of_dict_a:
        return False
    else:
        type_a = get_list_of_dict_type(list_of_dict_a)
        if key in type_a:
            return True

        return False


def union(list_of_dict_a, list_of_dict_b):
    if not is_type_equal_list_of_dict(list_of_dict_a, list_of_dict_b):
        raise ValueError("Type of list of dicts do not match.")

    return list_of_dict_a + list_of_dict_b


def product_dict(dict_a, dict_b):
    output_dict = dict_a.copy()
    for key in dict_b:
        output_dict[key] = dict_b[key]
    return output_dict


def product_list_of_dict(list_of_dict_a, list_of_dict_b):
    if is_type_conflict_list_of_dict(list_of_dict_a, list_of_dict_b):
        raise ValueError("Type of list of dicts conflict.")

    list_of_dict_output = []
    for dict_a in list_of_dict_a:
        for dict_b in list_of_dict_b:
            list_of_dict_output.append(product_dict(dict_a, dict_b))

    return list_of_dict_output


def product_list_of_dict_value(list_of_dict_a, key, value):

    dict_b = dict()
    dict_b[key] = value
    list_of_dict_output = []
    for dict_a in list_of_dict_a:
        list_of_dict_output.append(product_dict(dict_a, dict_b))

    return list_of_dict_output


def list_to_list_of_dict(key, list_input):
    list_of_dict = []
    for input_value in list_input:
        value_dict = dict()
        value_dict[key] = input_value
        list_of_dict.append(value_dict)

    return list_of_dict


def flatten_product(dictionary):
    product_list = [{}]
    for key in dictionary:
        pattern = re.compile("^_literal:")
        if pattern.search(key):
            new_key = key.replace("_literal:", "")
            product_list = product_list_of_dict_value(product_list, new_key, dictionary[key])
        elif key == "_product":
            product_list = product_list_of_dict(product_list, flatten_product(dictionary[key]))
        elif key == "_union":
            product_list = product_list_of_dict(product_list, flatten_union(dictionary[key]))
        elif type(dictionary[key]) == list:
            product_list = product_list_of_dict(product_list,
                                                list_to_list_of_dict(key, dictionary[key]))
        else:
            raise ValueError("Unsupported Product Element Found")
    return product_list


def flatten_union(list_of_dict):
    union_list = []
    for dictionary in list_of_dict:
        for key in dictionary:
            if key == "_comment":
                continue
            if key == "_product":
                union_list = union_list + flatten_product(dictionary[key])
            elif key == "_union":
                union_list = union_list + flatten_union(dictionary[key])
            elif key == "_identity":
                union_list = union_list + dictionary[key]
            else:
                raise ValueError("Union can only have _product, _union or _identity as keys")
    return union_list
