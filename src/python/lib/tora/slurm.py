# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import os
import tora.test
import time
import subprocess
import json


class executor:
    _task_array = []
    _options = []

    def __init__(self, task_array: list[tora.task.task], options):
        for task in task_array:
            task._command.insert(0, "srun")
        if options:
            self._options = options.split()
        self._task_array = task_array

    def execute(self, cmdline: tora.test.command_line, reporter_type):
        out_dir = cmdline.out_dir
        if not out_dir:
            out_dir = "log"

        os.makedirs(out_dir, exist_ok=True)

        task_file_path = out_dir + "/task_file.json"
        with open(task_file_path, 'w') as task_file:
            task_file.write(str(self._task_array))

        cmdline.launcher = tora.mila.launcher.shell
        cmdline.options = None
        arg_list = cmdline.to_arg_list()
        arg_list.insert(0, "tora-test")
        arg_list.append("--task-file")
        arg_list.append(task_file_path)
        run_command = ["salloc"]
        run_command += self._options
        run_command += arg_list
        print(run_command)
        result = subprocess.run(run_command)
        if result.returncode != 0:
            return False

        return True
