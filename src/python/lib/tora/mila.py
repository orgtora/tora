# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import concurrent.futures
import copy
import subprocess
import json
import time
import os
import math
import time
import termcolor
import enum


class launcher(enum.Enum):
    default = 0
    shell = 1
    slurm = 2
    srun = 3


def launcher_from_string(string):
    if string == "default":
        return launcher.default
    elif string == "shell":
        return launcher.shell
    elif string == "slurm":
        return launcher.slurm
    elif string == "sbatch":
        return launcher.sbatch
    else:
        raise RuntimeError("Unexpected launcher string provided: " + string)


def launcher_to_string(launcher_instance):
    if launcher_instance == launcher.default:
        return "default"
    elif launcher_instance == launcher.shell:
        return "shell"
    elif launcher_instance == launcher.slurm:
        return "slurm"
    elif launcher_instance == launcher.sbatch:
        return "sbatch"


class default_reporter:
    _output_param = {}
    _no_test = False
    _result_only = False

    def __init__(self, task_array, result_only):
        align = 2
        self._output_param["width"] = {}
        self._result_only = result_only

        if len(task_array) != 0:
            raw_name_width = max([len(task.name()) for task in task_array])
            self._output_param["width"]["name"] = raw_name_width + (align - raw_name_width % align)

            raw_id_width = int(math.log10(len(task_array))) + 3
            self._output_param["width"]["id"] = raw_id_width + (align - raw_id_width % align)

            raw_return_code_width = 3
            offset = align - raw_return_code_width % align
            self._output_param["width"]["return_code"] = raw_return_code_width + offset

            raw_signal_width = 3
            offset = align - raw_signal_width % align
            self._output_param["width"]["signal"] = raw_signal_width + offset
        else:
            self._no_test = True
            self._output_param["width"]["name"] = 0
            self._output_param["width"]["id"] = 0
            self._output_param["width"]["signal"] = 0

    def print_result_header(self, out_dir):
        id_width = self._output_param["width"]["id"]
        name_width = self._output_param["width"]["name"]

        print("\n Log Location: " + out_dir + "\n")

        if not self._no_test:
            fullwidth = id_width + name_width + 20
            print("".join([char * fullwidth for char in "-"]))
            string = str("Id").ljust(id_width, " ")
            string += str("Name").ljust(name_width, " ")

            string += str("Result").ljust(11, " ")
            string += str("Time").ljust(8, " ")

            print(string)
            print("".join([char * fullwidth for char in "-"]))

    def print_result_footer(self, out_dir, success, output_result):
        id_width = self._output_param["width"]["id"]
        name_width = self._output_param["width"]["name"]
        fullwidth = id_width + name_width + 20
        if not self._no_test:
            print("".join([char * fullwidth for char in "-"]))

        total_fail = 0
        total = len(output_result)
        for result in output_result:
            if not result.has_passed():
                total_fail = total_fail + 1

        if success:
            print("\n" + termcolor.colored("SUCCESS: ", "green") + str(total) + " Tests Passed")
        else:
            print(
                termcolor.colored("\n" + "FAILED: ", "red") + str(total_fail) +
                " tests failed out of " + str(total))

    def print_task_info(self, task):
        id_width = self._output_param["width"]["id"]
        name_width = self._output_param["width"]["name"]
        rc_width = self._output_param["width"]["return_code"]
        signal_width = self._output_param["width"]["signal"]

        string = str(task.id()).ljust(id_width, " ")
        string += str(task.name()).ljust(name_width, " ")

        print(string, end="")

    def print_task_result(self, task, result, stdout):
        id_width = self._output_param["width"]["id"]
        name_width = self._output_param["width"]["name"]
        rc_width = self._output_param["width"]["return_code"]
        signal_width = self._output_param["width"]["signal"]

        string = str(
            (lambda a: termcolor.colored("PASS", "green")
             if a else termcolor.colored("FAIL", "red"))(result.has_passed())).ljust(20, " ")
        string += str(str(round(result.time(), 2)).ljust(4, " ") + " sec").ljust(8, " ")

        print(string)

        if not result.has_passed() and not self._result_only:
            print("\n\tCommand: " + " ".join(task.command()))
            print("\tWorking Directory: " + task.working_directory())
            print("\tReturn Code: " + str(result.return_code()).ljust(4, " ") + "\tSignal: " +
                  str(result.signal()).ljust(4, " "))
            print("\tOutput:\n")
            print(stdout.decode("utf-8"))
            print("")
