# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------------------------

import argparse
import os
import tora.test
import time
import subprocess
import copy
import json


class executor:
    _task_array = []

    _pending_task_array = []
    _completed_task_array = []

    _success = True
    _output_result = []

    _result_only = False

    def __init__(self, task_array, options):
        self._task_array = task_array
        self._pending_task_array = task_array
        if options:
            options = options.lstrip('\"').rstrip('\"').split(' ')
            self.process_options(options)

    def process_options(self, options):
        exparser = argparse.ArgumentParser(description="Shell Options")
        exparser.add_argument("--result-only", help="Print Only Result", action='store_true')
        command_line = exparser.parse_args(options)
        self._result_only = command_line.result_only

    def is_workflow_complete(self):
        if len(self._pending_task_array) != 0:
            return False

        return True

    def find_next_task(self):
        return self._pending_task_array[0]

    def execute(self, cmdline: tora.test.command_line, reporter_type):
        out_dir = cmdline.out_dir

        os.makedirs(out_dir, exist_ok=True)
        task_file_path = out_dir + "/task_file.json"
        with open(task_file_path, 'w') as task_file:
            task_file.write(str(self._task_array))

        run_disabled = cmdline.run_disabled

        self._pending_task_array = self._task_array
        reporter = reporter_type(self._task_array, self._result_only)

        reporter.print_result_header(out_dir)
        if len(self._task_array) == 0:
            self._success = False

        while not self.is_workflow_complete():
            task = self.find_next_task()
            if task != None:
                run_task = False
                if task.is_disabled() and run_disabled:
                    run_task = True
                elif not task.is_disabled():
                    run_task = True

                if run_task:
                    task_out_dir = out_dir + "/" + str(task.id()) + "/"
                    result = self.execute_task(task, task_out_dir, reporter)
                    self._output_result.append(result)
                    if not result.has_passed():
                        self._success = False
                self._pending_task_array.remove(task)

        reporter.print_result_footer(out_dir, self._success, self._output_result)
        output_file = out_dir + "/report.json"

        with open(output_file, 'w') as report_file:
            output = [result.json() for result in self._output_result]
            report_file.write(json.dumps(output))

        return self._success

    def execute_task(self, task, out_dir, reporter):
        os.makedirs(out_dir, exist_ok=True)
        work_dir = task.working_directory()

        start_time = time.time()
        env = {}
        for var_name in os.environ:
            env[var_name] = copy.deepcopy(os.environ[var_name])
        for var_name in task.env():
            env[var_name] = task.env()[var_name]

        reporter.print_task_info(task)
        os_result = subprocess.run(task.command(),
                                   env=env,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT,
                                   cwd=work_dir)
        end_time = time.time()

        delta_t = end_time - start_time

        task_return_code = os_result.returncode
        if task_return_code < 0:
            signal = -task_return_code
            return_code = 0
        else:
            signal = 0
            return_code = task_return_code

        stdout_filename = os.path.join(out_dir, "stdout")
        with open(stdout_filename, "wb") as stdout_file:
            stdout_file.write(os_result.stdout)

        task_result = tora.task.result(task, return_code, signal, delta_t, os_result.stdout)
        if not task_result.has_passed() and not self._result_only:
            stdout = os_result.stdout
        else:
            stdout = bytes()

        reporter.print_task_result(task, task_result, stdout)
        return task_result
