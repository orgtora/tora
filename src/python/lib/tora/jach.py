# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import subprocess
import json
import re
import copy


class test:
    _id = -1
    _name = ""
    _command = []
    _working_directory = None
    _will_fail = False
    _label_array = []
    _dependent_test_array = []
    _is_disabled = False
    _env = {}

    def __init__(self, task_id, test_data_json, passthrough):
        self._id = task_id
        self._command = test_data_json["command"]
        self._name = test_data_json["name"]

        if passthrough:
            passthrough_text = "TORA_PASSTHROUGH"
            if passthrough_text in self._command:
                passthrough_index = self._command.index(passthrough_text)
                self._command.remove(passthrough_text)
                passthrough_array = passthrough.split(" ")
                for idx, arg in enumerate(passthrough_array):
                    self._command.insert(passthrough_index + idx, arg)

        for property in test_data_json["properties"]:
            if property["name"] == "WILL_FAIL":
                self._will_fail = property["value"]

        for property in test_data_json["properties"]:
            if property["name"] == "LABELS":
                self._label_array = property["value"]
            if property["name"] == "WORKING_DIRECTORY":
                self._working_directory = property["value"]
            if property["name"] == "DISABLED":
                if property["value"]:
                    self._is_disabled = True
            if property["name"] == "ENVIRONMENT":
                self._env = {}
                for env_variable in property["value"]:
                    env_name, env_value = env_variable.split('=', 1)
                    self._env[env_name] = copy.deepcopy(env_value)

    def id(self):
        return self._id

    def label_array(self):
        return self._label_array

    def name(self):
        return self._name

    def env(self):
        return self._env

    def will_fail(self):
        return self._will_fail

    def is_disabled(self):
        return self._is_disabled

    def command(self):
        return self._command

    def working_directory(self):
        return self._working_directory

    def dependent_test_array(self):
        return self._dependent_test_array

    def __str__(self):
        return json.dumps(self.json(), indent=2)

    def __repr__(self):
        return json.dumps(self.json(), indent=2)

    def json(self):
        value = {}
        value["name"] = self._name
        value["command"] = self._command
        value["working_directory"] = self._working_directory
        value["label_array"] = self._label_array
        value["env"] = self._env
        value["will_fail"] = self._will_fail
        value["id"] = self._id
        return value


def parse(source_dir, test_dir, passthrough, preset, label_regex_string, name_regex_string):
    command = ["ctest", "--show-only=json-v1", "--test-dir", test_dir]
    if preset != None:
        command = command + ["--preset", preset]

    result = subprocess.run(command, cwd=source_dir, capture_output=True, text=True)

    if result.returncode == 0:
        raw_test_data = json.loads(result.stdout)
        test_data = raw_test_data["tests"]
        test_array = []

        if not label_regex_string:
            label_regex_string = "(.*?)"

        for i, test_json in enumerate(test_data):
            single_test = test(i, test_json, passthrough)
            for label in single_test.label_array():
                if re.search(label_regex_string, label):
                    test_array.append(single_test)
                    break
            i = i + 1

        if name_regex_string:
            test_array_name_filtered = []
            for single_test in test_array:
                name = single_test.name()
                if re.search(name_regex_string, name):
                    test_array_name_filtered.append(single_test)
                i = i + 1
            return test_array_name_filtered

        return test_array

    elif result.returncode != 0:
        exception_message = "Extracting Test Data from CMake Failed with Error:\n" + result.stderr
        raise RuntimeError(exception_message)
