# ---------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ---------------------------------------------------------------------------------------------------
import argparse
import yaml
import tora
import json
import os
import copy


def compute_job_array(profile_data):
    job_array = []
    for key in profile_data:
        key_job_array = tora.cial.flatten_union(profile_data[key])
        for value in key_job_array:
            value["subprofile"] = key
        for job in key_job_array:
            job_array.append(job)

    return job_array


def verify_job_compatibility(job, platform):
    subprofile = job["subprofile"]
    platform_name = job["platform"]

    tmp_job = copy.deepcopy(job)
    del tmp_job["platform"]
    del tmp_job["subprofile"]
    del tmp_job["template"]
    if "stage" in tmp_job:
        del tmp_job["stage"]

    cap_dim = platform["capability"]["dimensions"]
    cap_val = platform["capability"]["value"]

    # Verify if all capabilities requested by job are supported
    for cap in tmp_job:
        if cap not in cap_dim:
            ex_msg = "A job in subprofile " + subprofile + "requires capability " + cap + ".\n"
            ex_msg = "Capability " + cap + " is unsupported in " + platform_name + "."
            raise RuntimeError(ex_msg)

    # Verify if the capability values are supported
    cap_combination_array = tora.cial.flatten_union(cap_val)
    if tmp_job not in cap_combination_array:
        exmsg = "Incompatible Job Found:\n"
        exmsg += json.dumps(job, indent=2)
        raise RuntimeError(exmsg)


def verify_ci_compatibility(platform_info, job_array):
    for job in job_array:
        if job["platform"] in platform_info:
            verify_job_compatibility(job, platform_info[job["platform"]])
        else:
            exception_message = "Unsupported platform " + job["platform"] + \
                    " requested in a job from subprofile " + job["subprofile"] + ".\n"
            exception_message += json.dumps(job)
            raise RuntimeError(exception_message)


def compute_job_name(job):
    cap_array = []

    for key in job:
        if key != "stage":
            cap_array.append(job[key].replace(";", "."))

    job_name = ".".join(cap_array).replace("..", ".")
    return job_name


def generate_ci_dict(job_array, platform_info):
    ci_dict = {}
    for job in job_array:
        job_name = compute_job_name(job)
        ci_dict[job_name] = generate_job_dict(job, platform_info)

    return ci_dict


def compute_platform_template(platform):
    return ".platform." + platform


def generate_job_dict(job, platform_info):
    job_dict = {}
    for entry, value in platform_info[job["platform"]]["gitlab"]["_all"].items():
        job_dict[entry] = copy.deepcopy(value)

    if job["JOB_FEATURE"] in platform_info[job["platform"]]["gitlab"]:
        for entry, value in platform_info[job["platform"]]["gitlab"][job["JOB_FEATURE"]].items():
            job_dict[entry] = copy.deepcopy(value)

    subprofile = job["subprofile"]
    platform_name = job["platform"]
    template = job["template"]

    tmp_job = copy.deepcopy(job)
    del tmp_job["platform"]
    del tmp_job["subprofile"]
    del tmp_job["template"]

    if "stage" in job:
        job_dict["stage"] = job["stage"]

    for cap in tmp_job:
        job_dict["variables"][cap] = copy.deepcopy(tmp_job[cap])
        job_dict["variables"]["JOB_SUBPROFILE"] = copy.deepcopy(subprofile)
        job_dict["variables"]["PLATFORM_NAME"] = copy.deepcopy(platform_name)
        job_dict["extends"] = [compute_platform_template(platform_name), template]
    return job_dict
