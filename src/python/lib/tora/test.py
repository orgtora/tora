# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import tora.mila
import argparse
import os


class command_line:
    launcher = tora.mila.launcher.default
    options = None
    passthrough = None
    test_dir = None
    source_dir = None
    out_dir = None
    timestamp = False
    label = None
    name = None
    run_disabled = False
    task_file = None
    preset = None

    def __init__(self, arg_array):
        parser = argparse.ArgumentParser(description="Test Runner")
        parser.add_argument("--launcher", default="default", help="Launcher", type=str)
        parser.add_argument("--options", default="", help="Launcher Options", type=str)
        parser.add_argument("--passthrough", help="Passthrough", default=None, type=str)
        parser.add_argument("--test-dir", help="Test Directory", required=True, type=str)
        parser.add_argument("--source-dir", help="Source Directory", default=os.getcwd(), type=str)
        parser.add_argument("--out-dir", help="Output Directory", default="log", type=str)
        parser.add_argument("--timestamp", help="Put Timestamps on Output", action='store_true')
        parser.add_argument("-L", "--label", help="Label Regex", default=None, type=str)
        parser.add_argument("--run-disabled", help="Run Disabled Tests", action='store_true')
        parser.add_argument("--preset",
                            help="Select a Certain Preset to get CMake Information",
                            default=None,
                            type=str)
        parser.add_argument("--task-file", help="Path to the Task File", default=None, type=str)
        parser.add_argument("--name", help="Name Regex", default=None, type=str)
        cmdline = parser.parse_args(arg_array)

        self.launcher = tora.mila.launcher_from_string(cmdline.launcher)
        self.options = cmdline.options
        self.passthrough = cmdline.passthrough
        self.test_dir = os.path.realpath(cmdline.test_dir)
        self.source_dir = os.path.realpath(cmdline.source_dir)

        if os.path.isabs(cmdline.out_dir):
            self.out_dir = cmdline.out_dir
        else:
            self.out_dir = cmdline.test_dir + "/log"

        self.task_file = cmdline.task_file

        self.timestamp = cmdline.timestamp
        self.label = cmdline.label
        self.name = cmdline.name
        self.run_disabled = cmdline.run_disabled
        self.preset = cmdline.preset

    def to_arg_list(self):
        arg_list = []

        if self.launcher != tora.mila.launcher.default:
            arg_list.append("--launcher")
            arg_list.append(tora.mila.launcher_to_string(self.launcher))

        if self.options:
            arg_list.append("--options")
            arg_list.append(self.options)

        if self.passthrough:
            arg_list.append("--passthrough")
            arg_list.append(self.passthrough)

        arg_list.append("--test-dir")
        arg_list.append(self.test_dir)

        arg_list.append("--source-dir")
        arg_list.append(self.source_dir)

        arg_list.append("--out-dir")
        arg_list.append(self.out_dir)

        if self.label:
            arg_list.append("--label")
            arg_list.append(self.label)

        if self.name:
            arg_list.append("--name")
            arg_list.append(self.name)

        arg_list.append("--run-disabled")

        if self.preset:
            arg_list.append("--preset")
            arg_list.append(self.preset)

        arg_list.append("--timestamp")

        return arg_list
