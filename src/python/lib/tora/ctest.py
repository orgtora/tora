# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------------------------

import subprocess
import json
import re


class test:
    _id = -1
    _name = ""
    _command: list[str] = []
    _working_directory = None
    _will_fail = False
    _label_array = []
    _dependent_test_array = []
    _is_disabled = False

    def __init__(self, task_id, test_data_json, passthrough):
        self._id = task_id
        self._command = test_data_json["command"]
        self._name = test_data_json["name"]

            passthrough_text = "TORA_PASSTHROUGH"
        if passthrough_text in self._command:
            passthrough_index = self._command.index(passthrough_text)
            self._command.remove(passthrough_text)
            if passthrough:
                passthrough_array = passthrough.split(" ")
                for idx, arg in enumerate(passthrough_array):
                    self._command.insert(passthrough_index + idx, arg)

        for property in test_data_json["properties"]:
            if property["name"] == "WILL_FAIL":
                self._will_fail = property["value"]

        for property in test_data_json["properties"]:
            if property["name"] == "LABELS":
                self._label_array = property["value"]
            if property["name"] == "WORKING_DIRECTORY":
                self._working_directory = property["value"]
            if property["name"] == "DISABLED":
                if property["value"]:
                    self._is_disabled = True

    def id(self):
        return self._id

    def label_array(self):
        return self._label_array

    def name(self):
        return self._name

    def will_fail(self):
        return self._will_fail

    def is_disabled(self):
        return self._is_disabled

    def command(self):
        return self._command

    def working_directory(self):
        return self._working_directory

    def dependent_test_array(self):
        return self._dependent_test_array

    def __str__(self):
        return self.json()

    def __repr__(self):
        return json.dumps(self.json(), indent=2)

    def json(self):
        value = {}
        value["name"] = self._name
        value["command"] = self._command
        value["working_directory"] = self._working_directory
        value["label_array"] = self._label_array
        value["will_fail"] = self._will_fail
        value["id"] = self._id
        return value


def parse(test_dir, passthrough, label_regex_string):
    result = subprocess.run(["ctest", "--show-only=json-v1"],
                            cwd=test_dir,
                            capture_output=True,
                            text=True)
    if result.returncode == 0:
        raw_test_data = json.loads(result.stdout)
        test_data = raw_test_data["tests"]
        test_array = []

        if not label_regex_string:
            label_regex_string = "(.*?)"

        label_regex = re.compile(label_regex_string)

        for i, test_json in enumerate(test_data):
            single_test = test(i, test_json, passthrough)
            for label in single_test.label_array():
                if re.match(label_regex, label):
                    test_array.append(single_test)
                    break
            i = i + 1
        return test_array

    elif result.returncode != 0:
        exception_message = "Extracting Test Data from CMake Failed with Error:\n" + result.stderr
        raise RuntimeError(exception_message)
