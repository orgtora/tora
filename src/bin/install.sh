# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set -euo pipefail

#set -x

DEFAULT_SOURCE_VERSION="main"

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

print_help() {
  printf "%6s\n" "usage:"
  printf "    %-30s %-20s\n"  "install.sh --help" "Display this help"
  printf "    %-30s %-20s\n"  "install.sh <args>" "Install Tora according to Options"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "[--prefix prefix]" "Install Prefix"
  printf "  %-40s %-30s\n"  "[--version version]" "Install Version (default: $DEFAULT_SOURCE_VERSION)"
  printf "  %-40s %-30s\n"  "[--tmp-dir tmpdir]" "Temporary Directory for Downloads (default: mktemp -d -p /tmp -t tora.XXXXXXXXXX) (Must be Specified with --only)"
  printf "  %-40s %-30s\n"  "[--only download | install ]" "Only Download or Install"
}

if [[ $# -eq 0 ]]; then
  print_help
  exit 1
fi

TMP_DIR_SPECIFIED="false"
INSTALL_PREFIX_SPECIFIED="false"
VERSION_SPECIFIED="false"
ONLY_SPECIFIED="false"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case $KEY in
    -h|--help)
      print_help
      exit 0
      ;;
    -p|--prefix)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      INSTALL_PREFIX=$1
      INSTALL_PREFIX_SPECIFIED="true"
      shift
      ;;
    --version)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      VERSION=$1
      VERSION_SPECIFIED="true"
      shift
      ;;
    --tmp-dir)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      TMP_DIR=$1
      TMP_DIR_SPECIFIED="true"
      shift
      ;;
    --only)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      ONLY="true"
      ONLY_SPECIFIED="true"
      shift
      ;;
    *)
      print_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $VERSION_SPECIFIED == "false" ]]; then
  VERSION=$DEFAULT_SOURCE_VERSION
fi

if [[ $TMP_DIR_SPECIFIED == "false" ]]; then
  TMP_DIR=$(mktemp -d -p /tmp -t tora.XXXXXXXXXX)
else
  mkdir -p $TMP_DIR
fi

if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  print_help
  exit 1
fi

DOWNLOAD_OPERATION="true"
INSTALL_OPERATION="true"

if [[ $ONLY_SPECIFIED == "true" ]]; then
  if [[ $ONLY == "download" ]]; then
    INSTALL_OPERATION = "false"
  fi

  if [[ $ONLY == "install" ]]; then
    DOWNLOAD_OPERATION = "false"
  fi
fi

if [[ $INSTALL_OPERATION == "true" ]]; then
  if [[ $INSTALL_PREFIX_SPECIFIED == "false" ]]; then
    echo "Missing install prefix."
    PRINT_HELP_AND_EXIT="true"
  fi

  mkdir -p  $INSTALL_PREFIX
  INSTALL_PREFIX=$(realpath $INSTALL_PREFIX)
fi


if [[ $DOWNLOAD_OPERATION == "true" ]]; then
  SOURCE_URL=https://orgtora.gitlab.io/releases/$VERSION/install.tar.gz
  pushd $TMP_DIR
  curl -L $SOURCE_URL -o tora.tar.gz
  popd
fi


if [[ $INSTALL_OPERATION == "true" ]]; then
  pushd $TMP_DIR
  tar -xf tora.tar.gz
  cp -rp tora/* $INSTALL_PREFIX/
  popd
fi



