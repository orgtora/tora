#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

find_program(
  TORA_VERSION_GIT git NO_DEFAULT_PATH PATHS ${TORA_BINARY_PATH_ARRAY}
  DOC "git executable for tora version module")

function(tora_version_tree_hash GIT_TREE_HASH)
  if(${TORA_VERSION_GIT} STREQUAL "TORA_VERSION_GIT-NOTFOUND")
    set(${GIT_TREE_HASH} "gitnotfound" PARENT_SCOPE)
  endif()

  execute_process(
    COMMAND ${TORA_VERSION_GIT} rev-parse --is-inside-work-tree
    OUTPUT_VARIABLE IS_INSIDE_GIT_WORKTREE ERROR_VARIABLE GIT_ERROR
    RESULT_VARIABLE GIT_RESULT
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

  if(${GIT_RESULT} STREQUAL "0")
    execute_process(
      COMMAND ${TORA_VERSION_GIT} rev-parse --short --verify HEAD
      OUTPUT_VARIABLE LOCAL_GIT_TREE_HASH ERROR_VARIABLE GIT_ERROR
      WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

    string(REGEX REPLACE "\n$" "" LOCAL_GIT_TREE_HASH "${LOCAL_GIT_TREE_HASH}")
    set(${GIT_TREE_HASH} "${LOCAL_GIT_TREE_HASH}" PARENT_SCOPE)
  else()
    set(${GIT_TREE_HASH} "reponotgit" PARENT_SCOPE)
  endif()

endfunction()

function(tora_version_branch GIT_BRANCH)
  if(${TORA_VERSION_GIT} STREQUAL "TORA_VERSION_GIT-NOTFOUND")
    set(${GIT_TREE_HASH} "gitnotfound" PARENT_SCOPE)
  endif()

  execute_process(
    COMMAND ${TORA_VERSION_GIT} rev-parse --is-inside-work-tree
    OUTPUT_VARIABLE IS_INSIDE_GIT_WORKTREE ERROR_VARIABLE GIT_ERROR
    RESULT_VARIABLE GIT_RESULT
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

  if(${GIT_RESULT} STREQUAL "0")
    execute_process(
      COMMAND ${TORA_VERSION_GIT} rev-parse --abbrev-ref HEAD
      OUTPUT_VARIABLE LOCAL_GIT_BRANCH ERROR_VARIABLE GIT_ERROR
      WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

    string(REGEX REPLACE "\n$" "" LOCAL_GIT_BRANCH "${LOCAL_GIT_BRANCH}")
    set(${GIT_BRANCH} "${LOCAL_GIT_BRANCH}" PARENT_SCOPE)
  else()
    set(${GIT_BRANCH} "reponotgit" PARENT_SCOPE)
  endif()

endfunction()
