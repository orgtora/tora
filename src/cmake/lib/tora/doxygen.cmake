#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

message(DEBUG "Checking tora::doxygen")
find_package(Doxygen REQUIRED dot)
function(tora_configure_doxygen DOCUMENTATION_TARGET DOXYGEN_OUTPUT_DIRECTORY
         DOXYFILE_IN)
  file(MAKE_DIRECTORY ${DOXYGEN_OUTPUT_DIRECTORY})
  set(DOXYFILE ${DOXYGEN_OUTPUT_DIRECTORY}/Doxyfile)
  configure_file(${DOXYFILE_IN} ${DOXYFILE} @ONLY)

  add_custom_target(
    tora.doxygen
    COMMAND Doxygen::doxygen ${DOXYFILE}
    WORKING_DIRECTORY ${DOXYGEN_OUTPUT_DIRECTORY}
    COMMENT "tora::doxygen: Generating API Documentation"
    VERBATIM)

  add_dependencies(${DOCUMENTATION_TARGET} tora.doxygen)
endfunction()

message(DEBUG "Checking tora::doxygen - Success")
