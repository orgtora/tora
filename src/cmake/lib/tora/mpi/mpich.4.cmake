# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
# Determine Link and Compile Options
# --------------------------------------------------------------------------------------------------

execute_process(
  COMMAND  ${TORA_MPICC} -show-compile-info
  OUTPUT_VARIABLE TORA_MPICC_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPICXX} -show-compile-info
  OUTPUT_VARIABLE TORA_MPICXX_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPIFORT} -compile-info
  OUTPUT_VARIABLE TORA_MPIFORT_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )


execute_process(
  COMMAND  ${TORA_MPICC} -show-link-info
  OUTPUT_VARIABLE TORA_MPICC_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPICXX} -show-link-info
  OUTPUT_VARIABLE TORA_MPICXX_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPIFORT} -link-info
  OUTPUT_VARIABLE TORA_MPIFORT_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )


# --------------------------------------------------------------------------------------------------
# Attach Link and Compile Options to the Target
# --------------------------------------------------------------------------------------------------

separate_arguments(TORA_MPICC_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPICC_COMPILE_OPTIONS}")
target_compile_options(tora::mpi_c INTERFACE ${TORA_MPICC_COMPILE_FLAGS})

separate_arguments(TORA_MPICC_LINK_FLAGS UNIX_COMMAND "${TORA_MPICC_LINK_OPTIONS}")
target_link_options(tora::mpi_c INTERFACE ${TORA_MPICC_LINK_FLAGS})

separate_arguments(TORA_MPICXX_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPICXX_COMPILE_OPTIONS}")
target_compile_options(tora::mpi_cxx INTERFACE ${TORA_MPICXX_COMPILE_FLAGS})

separate_arguments(TORA_MPICXX_LINK_FLAGS UNIX_COMMAND "${TORA_MPICXX_LINK_OPTIONS}")
target_link_options(tora::mpi_cxx INTERFACE ${TORA_MPICXX_LINK_FLAGS})

separate_arguments(TORA_MPIFORT_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPIFORT_COMPILE_OPTIONS}")
list(FILTER TORA_MPIFORT_COMPILE_FLAGS INCLUDE REGEX "^-I")
target_compile_options(tora::mpi_fort INTERFACE ${TORA_MPIFORT_COMPILE_FLAGS})

separate_arguments(TORA_MPIFORT_LINK_FLAGS UNIX_COMMAND "${TORA_MPIFORT_LINK_OPTIONS}")
list(FILTER TORA_MPIFORT_LINK_FLAGS EXCLUDE REGEX "^-I")
list(FILTER TORA_MPIFORT_LINK_FLAGS EXCLUDE REGEX "^gfortran$")
target_link_options(tora::mpi_fort INTERFACE ${TORA_MPIFORT_LINK_FLAGS})

