# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------------------
# Determine Link and Compile Options
# --------------------------------------------------------------------------------------------------

execute_process(
  COMMAND  ${TORA_MPICC} --showme:compile
  OUTPUT_VARIABLE TORA_MPICC_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPICXX} --showme:compile
  OUTPUT_VARIABLE TORA_MPICXX_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPIFORT} --showme:compile
  OUTPUT_VARIABLE TORA_MPIFORT_COMPILE_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )


execute_process(
  COMMAND  ${TORA_MPICC} --showme:link
  OUTPUT_VARIABLE TORA_MPICC_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPICXX} --showme:link
  OUTPUT_VARIABLE TORA_MPICXX_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND  ${TORA_MPIFORT} --showme:link
  OUTPUT_VARIABLE TORA_MPIFORT_LINK_OPTIONS
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )


# --------------------------------------------------------------------------------------------------
# Attach Link and Compile Options to the Target
# --------------------------------------------------------------------------------------------------

separate_arguments(TORA_MPICC_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPICC_COMPILE_OPTIONS}")
target_compile_options(tora::mpi_c INTERFACE $<$<COMPILE_LANGUAGE:C>:${TORA_MPICC_COMPILE_FLAGS}>)

separate_arguments(TORA_MPICC_LINK_FLAGS UNIX_COMMAND "${TORA_MPICC_LINK_OPTIONS}")
target_link_options(tora::mpi_c INTERFACE ${TORA_MPICC_LINK_FLAGS})

separate_arguments(TORA_MPICXX_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPICXX_COMPILE_OPTIONS}")
target_compile_options(tora::mpi_cxx INTERFACE
  $<$<COMPILE_LANGUAGE:CXX>:${TORA_MPICXX_COMPILE_FLAGS}>)

separate_arguments(TORA_MPICXX_LINK_FLAGS UNIX_COMMAND "${TORA_MPICXX_LINK_OPTIONS}")
target_link_options(tora::mpi_cxx INTERFACE ${TORA_MPICXX_LINK_FLAGS})

separate_arguments(TORA_MPIFORT_COMPILE_FLAGS UNIX_COMMAND "${TORA_MPIFORT_COMPILE_OPTIONS}")
target_compile_options(tora::mpi_fort INTERFACE
  $<$<COMPILE_LANGUAGE:Fortran>:${TORA_MPIFORT_COMPILE_FLAGS}>)

separate_arguments(TORA_MPIFORT_LINK_FLAGS UNIX_COMMAND "${TORA_MPIFORT_LINK_OPTIONS}")
target_link_options(tora::mpi_fort INTERFACE ${TORA_MPIFORT_LINK_FLAGS})


