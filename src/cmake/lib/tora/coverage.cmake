#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

include_guard(GLOBAL)
message(DEBUG "Enabling tora::coverage")

if(TARGET tora.coverage)
  return()
else()
  add_custom_target(tora.coverage)
endif()

set(TORA_COVERAGE_OUTDIR ${PROJECT_BINARY_DIR}/coverage
    CACHE PATH "Path to Store Output of Coverage Data")

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/clang.cmake)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/gcc.cmake)
else()
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/dummy.cmake)
endif()
message(DEBUG "Enabling tora::coverage - Done")
