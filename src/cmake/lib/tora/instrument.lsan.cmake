#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::instrument.lsan.c INTERFACE IMPORTED)
add_library(tora::instrument.lsan.cxx INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::instrument.lsan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize=leak>)
  target_link_options(tora::instrument.lsan.c INTERFACE -fsanitize=leak)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GCC")
  target_compile_options(tora::instrument.lsan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize=leak>)

  target_link_options(tora::instrument.lsan.c INTERFACE -fsanitize=leak)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::instrument.lsan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=leak>)

  target_link_options(tora::instrument.lsan.cxx INTERFACE -fsanitize=leak)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GCC")
  target_compile_options(tora::instrument.lsan.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=leak>)
  target_link_options(tora::instrument.lsan.cxx INTERFACE -fsanitize=leak)
endif()
