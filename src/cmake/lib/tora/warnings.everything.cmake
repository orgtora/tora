#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::warning.everything.c INTERFACE IMPORTED)
add_library(tora::warning.everything.cxx INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Weverything>)
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-c++98-compat>)
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-c++20-compat>)
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-c++98-compat-pedantic>)

  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-covered-switch-default>)

  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-padded>)

  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-weak-vtables>)

  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-exit-time-destructors>)
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wno-global-constructors>)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wall>)
  target_compile_options(tora::warning.everything.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wextra>)
endif()


if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Weverything>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++98-compat>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++20-compat>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++98-compat-pedantic>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-covered-switch-default>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-padded>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-weak-vtables>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-exit-time-destructors>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wno-global-constructors>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wall>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wextra>)
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wpedantic>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "NVHPC")
  target_compile_options(tora::warning.everything.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wpedantic>)
endif()


