#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::warning.error.c INTERFACE IMPORTED)
add_library(tora::warning.error.cxx INTERFACE IMPORTED)
add_library(tora::warning.error.cuda INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.error.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Werror>)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.error.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Werror>)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.error.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Werror>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.error.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Werror>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "NVHPC")
  target_compile_options(tora::warning.error.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Werror>)
endif()


target_compile_options(tora::warning.error.cuda INTERFACE $<$<COMPILE_LANGUAGE:CUDA>:-Werror>)
