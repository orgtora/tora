# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------
include_guard(GLOBAL)

function(tora_mode_check_validity MODE_NAME)
  set(MODE_VALUE ${${MODE_NAME}})

  if(NOT ${MODE_VALUE} IN_LIST ${MODE_NAME}_ARRAY)
    message(
      FATAL_ERROR
        "
      Invalid ${MODE_NAME} Mode ${${MODE_NAME}}.
      Valid values are: ${${MODE_NAME}_ARRAY}
      ")
  endif()
endfunction()

function(tora_mode_help)
  message("Incorrect Usage of tora_mode. Correct Usage is:")
  message(
    FATAL_ERROR
      "
    tora_mode(
      <NAME>
      DESCRIPTION <DESCRIPTION>
      VALUE <ARRAY_OF_VALUES>
      DEFAULT <DEFAULT_VALUE>
      )
    ")
endfunction()

function(
  tora_mode
  MODE_NAME
  DESCRIPTION_NAME
  DESCRIPTION_VALUE
  VALUE_NAME
  MODE_ARRAY
  DEFAULT_NAME
  DEFAULT_MODE)

  if(NOT (${DESCRIPTION_NAME} STREQUAL "DESCRIPTION"))
    tora_mode_help()
  endif()

  if(NOT (${VALUE_NAME} STREQUAL "VALUE"))
    tora_mode_help()
  endif()

  if(NOT (${DEFAULT_NAME} STREQUAL "DEFAULT"))
    tora_mode_help()
  endif()

  set(${MODE_NAME} ${DEFAULT_MODE} CACHE STRING "${DESCRIPTION_VALUE}")
  set(${MODE_NAME} ${${MODE_NAME}} PARENT_SCOPE)

  set(${MODE_NAME}_ARRAY ${MODE_ARRAY})
  set(${MODE_NAME}_ARRAY ${${MODE_NAME}_ARRAY} PARENT_SCOPE)

  set(${MODE_NAME}_HUMAN_NAME ${MODE_HUMAN_NAME})
  set(${MODE_NAME}_HUMAN_NAME ${${MODE_NAME}_HUMAN_NAME} PARENT_SCOPE)

  tora_mode_check_validity(${MODE_NAME})
endfunction()
