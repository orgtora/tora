#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::instrument.ubsan.c INTERFACE IMPORTED)
add_library(tora::instrument.ubsan.cxx INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")

  target_compile_options(tora::instrument.ubsan.c INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=undefined>
    $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=undefined>
    $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>)

  target_link_options(tora::instrument.ubsan.c INTERFACE -fsanitize=undefined)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-sanitize-recover=undefined)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-optimize-sibling-calls)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GCC")

  target_compile_options(tora::instrument.ubsan.c INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=undefined>
    $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=undefined>
    $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>)

  target_link_options(tora::instrument.ubsan.c INTERFACE -fsanitize=undefined)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-sanitize-recover=undefined)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.ubsan.c INTERFACE -fno-optimize-sibling-calls)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::instrument.ubsan.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=undefined>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=undefined>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>)


  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fsanitize=undefined)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-sanitize-recover=undefined)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-optimize-sibling-calls)

elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GCC")

  target_compile_options(tora::instrument.ubsan.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=undefined>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=undefined>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>)

  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fsanitize=undefined)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-sanitize-recover=undefined)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.ubsan.cxx INTERFACE -fno-optimize-sibling-calls)
endif()
