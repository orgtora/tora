#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::warning.mandatory.c INTERFACE IMPORTED)
add_library(tora::warning.mandatory.cxx INTERFACE IMPORTED)


if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.mandatory.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wall>)
  target_compile_options(tora::warning.mandatory.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wextra>)
  target_compile_options(tora::warning.mandatory.c INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated-declarations>)

  target_compile_options(tora::warning.mandatory.c INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated>)

elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.mandatory.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wall>)
  target_compile_options(tora::warning.mandatory.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wextra>)

  target_compile_options(tora::warning.mandatory.c INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated-declarations>)

  target_compile_options(tora::warning.mandatory.c INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated>)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.mandatory.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wall>)
  target_compile_options(tora::warning.mandatory.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wextra>)

  target_compile_options(tora::warning.mandatory.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated-declarations>)

  target_compile_options(tora::warning.mandatory.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated>)

elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.mandatory.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wall>)
  target_compile_options(tora::warning.mandatory.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wextra>)

  target_compile_options(tora::warning.mandatory.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated-declarations>)

  target_compile_options(tora::warning.mandatory.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated>)

elseif(CMAKE_CXX_COMPILER_ID STREQUAL "NVHPC")
endif()
