# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------

include_guard(GLOBAL)

function(tora_feature_set_check_validity FEATURE_SET_NAME)
  set(FEATURE_SET_VALUE ${${FEATURE_SET_NAME}})

  foreach(FEATURE ${FEATURE_SET_VALUE})
    if(NOT ${FEATURE} IN_LIST ${FEATURE_SET_NAME}_ARRAY)
      message(
        FATAL_ERROR
        "
        Invalid ${FEATURE_SET_NAME} Feature ${FEATURE}.
        Valid values are: none,${${FEATURE_SET_NAME}_ARRAY},all
        "
        )
    endif()
  endforeach()
endfunction()

function(tora_feature_set_help)
  message("Incorrect Usage of tora_feature_set. Correct Usage is:")
  message(
    FATAL_ERROR
    "
    tora_feature_set(
      <NAME>
      DESCRIPTION <DESCRIPTION>
      VALUE <ARRAY_OF_VALUES>
      DEFAULT <DEFAULT_VALUE_ARRAY>
      )
    "
    )
endfunction()

function(
    tora_feature_set
    FEATURE_SET_NAME
    DESCRIPTION_NAME
    DESCRIPTION_VALUE
    VALUE_NAME
    FEATURE_SET_ARRAY
    DEFAULT_NAME
    DEFAULT_FEATURE_SET)

  if(NOT (${DESCRIPTION_NAME} STREQUAL "DESCRIPTION"))
    tora_feature_set_help()
  endif()

  if(NOT (${VALUE_NAME} STREQUAL "VALUE"))
    tora_feature_set_help()
  endif()

  if(NOT (${DEFAULT_NAME} STREQUAL "DEFAULT"))
    tora_feature_set_help()
  endif()

  set(${FEATURE_SET_NAME} "${DEFAULT_FEATURE_SET}" CACHE STRING
    "${DESCRIPTION_NAME}")

  set(${FEATURE_SET_NAME} ${${FEATURE_SET_NAME}} PARENT_SCOPE)

  set(${FEATURE_SET_NAME}_ARRAY ${FEATURE_SET_ARRAY})
  set(${FEATURE_SET_NAME}_ARRAY ${${FEATURE_SET_NAME}_ARRAY} PARENT_SCOPE)

  set(${FEATURE_SET_NAME}_HUMAN_NAME ${FEATURE_SET_NAME})
  set(${FEATURE_SET_NAME}MODE_HUMAN_NAME ${${FEATURE_SET_NAME}_HUMAN_NAME}
    PARENT_SCOPE)

  if("none" IN_LIST ${FEATURE_SET_NAME})

    list(LENGTH ${FEATURE_SET_NAME} FEATURE_LENGTH)
    if(NOT (${FEATURE_LENGTH} EQUAL 1))
      message(
        FATAL_ERROR
        "
        none is mutually exclusive with other ${FEATURE_SET_NAME} features.
        "
        )

    endif()

  elseif("all" IN_LIST ${FEATURE_SET_NAME})

    list(LENGTH ${FEATURE_SET_NAME} FEATURE_LENGTH)
    if(NOT (${FEATURE_LENGTH} EQUAL 1))

      message(
        FATAL_ERROR
        "
        all is mutually exclusive with other ${FEATURE_SET_NAME} features.
        "
        )
    endif()

    foreach(FEATURE ${FEATURE_SET_ARRAY})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} TRUE PARENT_SCOPE)
    endforeach()

  else()
    tora_feature_set_check_validity(${FEATURE_SET_NAME})
    foreach(FEATURE ${FEATURE_SET_ARRAY})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} FALSE PARENT_SCOPE)
    endforeach()

    foreach(FEATURE ${${FEATURE_SET_NAME}})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} TRUE PARENT_SCOPE)
    endforeach()
  endif()

endfunction()
