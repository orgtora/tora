# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------
include_guard(GLOBAL)

include(CMakeDependentOption)

set(TORABUILD_NOPREFIX OFF
  CACHE BOOL "Make torabuild commands available without `tora_` prefix")

set(TORA_USE_TORA_TEST ON CACHE BOOL "Use Tora Test Runner Configuration by Default")

if(TORA_USE_TORA_TEST)
  set(TORA_PASS_ARGS "TORA_PASSTHROUGH")
else()
  set(TORA_PASS_ARGS_RAW ${TORA_PASSTHROUGH})
  string(REPLACE " " ";" TORA_PASS_ARGS  ${TORA_PASS_ARGS_RAW})
endif()

function(tora_dev_option OPTION_NAME DESCRIPTION DEPVALUE)
  cmake_dependent_option(
    ${OPTION_NAME}
    ${DESCRIPTION}
    ${DEPVALUE}
    PROJECT_DEVBUILD
    OFF)
endfunction()

function(tora_target_source_directory TARGETNAME LINK_TYPE DIR_NAME)
  file(GLOB_RECURSE SOURCE_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${CMAKE_CURRENT_LIST_DIR} CONFIGURE_DEPENDS ${DIR_NAME}/*)
  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    target_sources(${TARGETNAME} ${LINK_TYPE} ${SRC_FILE})
  endforeach()
endfunction()

function(tora_target_header_directory TARGETNAME LINK_TYPE DIR_NAME)
  file(GLOB_RECURSE HEADER_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${CMAKE_CURRENT_LIST_DIR} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(
    REAL_PATH
    ${DIR_NAME}
    ABS_DIR_NAME
    BASE_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR})
  target_sources(
    ${TARGETNAME}
    ${LINK_TYPE}
    FILE_SET
    HEADERS
    BASE_DIRS
    ${ABS_DIR_NAME}
    FILES
    ${HEADER_FILE_LIST})
endfunction()

function(tora_add_unit_test_directory DIR_NAME TARGET_LIB_LIST PREFIX LABELNAME)
  file(
    REAL_PATH
    ${DIR_NAME}
    ABS_DIR_NAME
    BASE_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR})
  file(GLOB SOURCE_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${ABS_DIR_NAME} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(
    RELATIVE_PATH
    REL_DIR_NAME
    ${CMAKE_CURRENT_LIST_DIR}
    ${ABS_DIR_NAME}
    )





  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SRC_FILE})
    string(REGEX REPLACE "/" "." TARGETNAME ${TARGETNAME})
    set(TARGETNAME "unit.${PREFIX}.${REL_DIR_NAME}.${TARGETNAME}")

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SRC_FILE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})


    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SRC_FILE})
    add_test(
      NAME ${TARGETNAME}
      COMMAND ${TARGETNAME} ${TORA_PASS_ARGS}
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
      )
    foreach(TARGET_LIB ${TARGET_LIB_LIST})
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()
    if(TARGETNAME MATCHES ".fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES WILL_FAIL TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()


    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})

    tora_target_source_directory(${TARGETNAME} PRIVATE ${REL_DIR_NAME}/${TESTDIRNAME})
    tora_coverage_register_test(unit_test ${TARGETNAME} ${TARGETNAME})
  endforeach()
endfunction()

function(tora_add_integrated_unit_test_directory DIR_NAME TARGET_LIB_LIST
    LABELNAME)
  file(
    REAL_PATH
    ${DIR_NAME}
    ABS_DIR_NAME
    BASE_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR}
    )

  file(
    GLOB SOURCE_FILE_LIST LIST_DIRECTORIES false RELATIVE ${ABS_DIR_NAME}
    CONFIGURE_DEPENDS ${DIR_NAME}/*
    )

  file(
    RELATIVE_PATH
    REL_DIR_NAME
    ${CMAKE_CURRENT_LIST_DIR}
    ${ABS_DIR_NAME}
    )

  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SRC_FILE})
    string(REGEX REPLACE "/" "." TARGETNAME ${TARGETNAME})
    set(TARGETNAME "integration.${REL_DIR_NAME}.${TARGETNAME}")

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SRC_FILE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})

    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SRC_FILE})
    add_test(
      NAME ${TARGETNAME}
      COMMAND ${TARGETNAME} ${TORA_PASS_ARGS}
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
      )
    foreach(TARGET_LIB IN LISTS TARGET_LIB_LIST)
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()
    if(TARGETNAME MATCHES ".fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES WILL_FAIL TRUE)
    endif()
    if(TARGETNAME MATCHES "disabled.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})

    tora_target_source_directory(${TARGETNAME} PRIVATE ${REL_DIR_NAME}/${TESTDIRNAME})
    tora_coverage_register_test(integration_test ${TARGETNAME} ${TARGETNAME})
  endforeach()
endfunction()

function(tora_add_compile_test_directory DIR_NAME TARGET_LIB_LIST PREFIX LABELNAME)
  file(
    REAL_PATH
    ${DIR_NAME}
    ABS_DIR_NAME
    BASE_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR})
  file(GLOB SOURCE_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${ABS_DIR_NAME} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(
    RELATIVE_PATH
    REL_DIR_NAME
    ${CMAKE_CURRENT_LIST_DIR}
    ${ABS_DIR_NAME}
    )


  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SRC_FILE})
    string(REGEX REPLACE "/" "." TARGETNAME ${TARGETNAME})
    set(TARGETNAME "compile.${PREFIX}.${REL_DIR_NAME}.${TARGETNAME}")
    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SRC_FILE})

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SRC_FILE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})

    foreach(TARGET_LIB IN LISTS TARGET_LIB_LIST)
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()
    set_target_properties(
      ${TARGETNAME} PROPERTIES EXCLUDE_FROM_ALL TRUE EXCLUDE_FROM_DEFAULT_BUILD
      TRUE)

    add_test(NAME ${TARGETNAME} COMMAND ${CMAKE_COMMAND} --build . --target
      ${TARGETNAME} --config $<CONFIG>
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})

    if(TARGETNAME MATCHES ".fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES WILL_FAIL TRUE)
    endif()
    if(TARGETNAME MATCHES "disabled.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    tora_target_source_directory(${TARGETNAME} PRIVATE ${REL_DIR_NAME}/${TESTDIRNAME})
  endforeach()
endfunction()

function(tora_add_micro_bench_directory DIR_NAME TARGET_LIB_LIST PREFIX LABELNAME)
  file(
    REAL_PATH
    ${DIR_NAME}
    ABS_DIR_NAME
    BASE_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR})

  file(GLOB SOURCE_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${ABS_DIR_NAME} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(
    RELATIVE_PATH
    REL_DIR_NAME
    ${CMAKE_CURRENT_LIST_DIR}
    ${ABS_DIR_NAME}
    )

  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SRC_FILE})
    string(REGEX REPLACE "/" "." TARGETNAME ${TARGETNAME})
    set(TARGETNAME "micro.${PREFIX}.${REL_DIR_NAME}.${TARGETNAME}")

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SRC_FILE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})

    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SRC_FILE})

    add_test(
      NAME ${TARGETNAME}
      COMMAND ${TARGETNAME} ${TORA_PASS_ARGS}
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
      )
    if(TARGETNAME MATCHES "disabled.b$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    foreach(TARGET_LIB IN LISTS TARGET_LIB_LIST)
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()

    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})
    tora_target_source_directory(${TARGETNAME} PRIVATE ${REL_DIR_NAME}/${TESTDIRNAME})
  endforeach()
endfunction()

if(TORABUILD_NOPREFIX)
  function(target_header_directory TARGETNAME LINK_TYPE DIR_NAME)
    tora_target_header_directory(${TARGETNAME} ${LINK_TYPE} ${DIR_NAME})
  endfunction()

  function(target_source_directory TARGETNAME LINK_TYPE DIR_NAME)
    tora_target_source_directory(${TARGETNAME} ${LINK_TYPE} ${DIR_NAME})
  endfunction()

  function(add_unit_test_directory DIR_NAME TARGET_LIB PREFIX LABELNAME)
    tora_add_unit_test_directory(${DIR_NAME} ${TARGET_LIB} ${PREFIX} ${LABELNAME})
  endfunction()

  function(add_micro_bench_directory DIR_NAME TARGET_LIB PREFIX LABELNAME)
    tora_add_micro_bench_directory(${DIR_NAME} ${TARGET_LIB} ${PREFIX} ${LABELNAME})
  endfunction()

  function(add_integrated_unit_test_directory DIR_NAME TARGET_LIB LABELNAME)
    tora_add_integrated_unit_test_directory(${DIR_NAME} ${TARGET_LIB}
      ${LABELNAME})
  endfunction()

  function(add_compile_test_directory DIR_NAME TARGET_LIB PREFIX LABELNAME)
    tora_add_compile_test_directory(${DIR_NAME} ${TARGET_LIB} ${PREFIX} ${LABELNAME})
  endfunction()

  function(dev_option OPTION_NAME DESCRIPTION DEPVALUE)
    tora_dev_option(${OPTION_NAME} ${DESCRIPTION} ${DEPVALUE})
  endfunction()
endif()
