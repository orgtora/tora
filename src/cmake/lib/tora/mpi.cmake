#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

if(TORA_MPI_ROOT)
  set(TORA_MPI_BINARY_PATH_ARRAY "${TORA_MPI_ROOT}/bin")
else()
  set(TORA_MPI_BINARY_PATH_ARRAY ${TORA_BINARY_PATH_ARRAY})
endif()

find_program(TORA_MPICC mpicc REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_MPI_BINARY_PATH_ARRAY} DOC "mpicc")

find_program(TORA_MPICXX mpicxx REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_MPI_BINARY_PATH_ARRAY} DOC "mpicxx")

find_program(TORA_MPIFORT mpifort REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_MPI_BINARY_PATH_ARRAY} DOC "mpifort")

add_library(tora::mpi_c INTERFACE IMPORTED)
add_library(tora::mpi_cxx INTERFACE IMPORTED)
add_library(tora::mpi_fort INTERFACE IMPORTED)

execute_process(
  COMMAND
  ${TORA_MPICC} ${CMAKE_CURRENT_LIST_DIR}/mpi/probe.c -o ${CMAKE_CURRENT_BINARY_DIR}/mpi_prober
  COMMAND_ERROR_IS_FATAL ANY
  )


execute_process(
  COMMAND ${CMAKE_CURRENT_BINARY_DIR}/mpi_prober --vendor
  OUTPUT_VARIABLE TORA_MPI_VENDOR
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )


execute_process(
  COMMAND ${CMAKE_CURRENT_BINARY_DIR}/mpi_prober --version
  OUTPUT_VARIABLE TORA_MPI_VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

execute_process(
  COMMAND ${CMAKE_CURRENT_BINARY_DIR}/mpi_prober --subversion
  OUTPUT_VARIABLE TORA_MPI_SUBVERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
  )

if(${TORA_MPI_VENDOR} STREQUAL "openmpi")
  include(${CMAKE_CURRENT_LIST_DIR}/mpi/openmpi.cmake)
elseif(${TORA_MPI_VENDOR} STREQUAL "mpich")
  if(${TORA_MPI_VERSION} LESS 4)
    include(${CMAKE_CURRENT_LIST_DIR}/mpi/mpich.cmake)
  elseif(${TORA_MPI_VERSION} GREATER_EQUAL 4)
    include(${CMAKE_CURRENT_LIST_DIR}/mpi/mpich.4.cmake)
  endif()
else()
  message(FATAL_ERROR "Unsupported vendor ${TORA_MPI_VENDOR} detected.")
endif()
