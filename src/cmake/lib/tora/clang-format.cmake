# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------

include_guard(GLOBAL)
message(DEBUG "Enabling tora::clang-format")

add_custom_target(tora.format.check)
add_custom_target(tora.format.check.full)

find_program(TORA_FORMAT tora-format REQUIRED NO_DEFAULT_PATH
             PATHS ${TORA_BINARY_PATH_ARRAY} DOC "tora main executable")

find_program(TORA_CLANG_FORMAT clang-format REQUIRED NO_DEFAULT_PATH
             PATHS ${TORA_BINARY_PATH_ARRAY} DOC "tora clang-format")

add_custom_command(
  OUTPUT ${PROJECT_BINARY_DIR}/tora.format.check.phony
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  COMMAND ${TORA_FORMAT} --clang-format ${TORA_CLANG_FORMAT} --source-dir
          ${PROJECT_SOURCE_DIR} --check)

add_custom_target(
  tora.format.check.run DEPENDS ${PROJECT_BINARY_DIR}/tora.format.check.phony
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

add_dependencies(tora.format.check tora.format.check.run)

add_custom_command(
  OUTPUT ${PROJECT_BINARY_DIR}/tora.format.check.full.phony
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  COMMAND ${TORA_FORMAT} --clang-format ${TORA_CLANG_FORMAT} --source-dir
          ${PROJECT_SOURCE_DIR} --check --full)

add_custom_target(
  tora.format.check.full.run
  DEPENDS ${PROJECT_BINARY_DIR}/tora.format.check.full.phony
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

add_dependencies(tora.format.check.full tora.format.check.full.run)

message(DEBUG "Enabling tora::clang-format - Done")
