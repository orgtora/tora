#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

function(vanmake_add_directory DIR_NAME GENCODE_DIRECTORY)
  file(GLOB_RECURSE SOURCE_FILE_LIST LIST_DIRECTORIES false
       RELATIVE ${CMAKE_CURRENT_LIST_DIR} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(REAL_PATH ${DIR_NAME} ABS_DIR)
  file(RELATIVE_PATH REL_DIR ${PROJECT_SOURCE_DIR} ${ABS_DIR})
  set(GENCODE_DIRECTORY ${PROJECT_BINARY_DIR}/gencode/${REL_DIR})
  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    file(RELATIVE_PATH SOURCE_RELATIVE_PATH ${DIR_NAME} ${SRC_FILE})
    set(GENERATED_FILE ${GENCODE_DIRECTORY}/${SOURCE_RELATIVE_PATH})
    string(REGEX REPLACE ".vanmake$" "" GENERATED_FILE ${GENERATED_FILE})
    configure_file(${SRC_FILE} ${GENERATED_FILE} @ONLY)
  endforeach()

  set(GENCODE_DIRECTORY ${GENCODE_DIRECTORY} PARENT_SCOPE)
endfunction()
