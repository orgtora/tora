# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------
include(CMakePushCheckState)
cmake_push_check_state()

set(CMAKE_REQUIRED_QUIET ${tora_FIND_QUIETLY})
set(want_components ${tora_FIND_COMPONENTS})

set(extra_components ${want_components})

list(
  REMOVE_ITEM
  extra_components
  clang-tidy
  clang-format
  coverage
  coverage.dummy
  doxygen
  explorer
  feature-set
  instrument.asan
  instrument.lsan
  instrument.ubsan
  mode
  mpi
  surrogate
  torabuild
  vanmake
  version
  warnings.deprecated
  warnings.error
  warnings.essential
  warnings.everything
  warnings.mandatory
  warnings.none
  warnings.recommended)

foreach(component IN LISTS extra_components)
  message(FATAL_ERROR "Invalid find_package component for tora: ${component}")
endforeach()

include(${CMAKE_CURRENT_LIST_DIR}/common.cmake)

foreach(component IN LISTS want_components)
  include(${CMAKE_CURRENT_LIST_DIR}/${component}.cmake)
endforeach()

cmake_pop_check_state()
