#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

include_guard(GLOBAL)

if(TARGET tora.clang-tidy)
  return()
else()
  add_custom_target(tora.clang-tidy)
endif()

set(TORA_CLANG_TIDY_PASSTHROUGH "" CACHE STRING
                                         "Passthrough Arguments for Clang Tidy")

find_program(TORA_RUN_CLANG_TIDY run-clang-tidy REQUIRED NO_DEFAULT_PATH
             PATHS ${TORA_BINARY_PATH_ARRAY} DOC "llvm-profdata executable")

find_program(TORA_CLANG_TIDY clang-tidy REQUIRED NO_DEFAULT_PATH
             PATHS ${TORA_BINARY_PATH_ARRAY} DOC "llvm-profdata executable")

add_custom_command(
  OUTPUT clang-tidy.run
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  COMMAND ${TORA_RUN_CLANG_TIDY} -clang-tidy-binary ${TORA_CLANG_TIDY}
          -extra-arg="${TORA_CLANG_TIDY_PASSTHROUGH}" -p ${PROJECT_BINARY_DIR}
  DEPENDS ${PROJECT_BINARY_DIR}/compile_commands.json)

add_custom_target(tora.clang-tidy.run DEPENDS clang-tidy.run
                  WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

add_dependencies(tora.clang-tidy tora.clang-tidy.run)
