#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

set(TORA_BINARY_PATH_ARRAY ${CMAKE_SYSTEM_PREFIX_PATH})
list(PREPEND TORA_BINARY_PATH_ARRAY ${CMAKE_PREFIX_PATH})
list(TRANSFORM TORA_BINARY_PATH_ARRAY APPEND "/bin")

