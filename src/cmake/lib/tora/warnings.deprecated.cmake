#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::warning.deprecated.c INTERFACE IMPORTED)
add_library(tora::warning.deprecated.cxx INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.deprecated.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wdeprecated>)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.deprecated.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wdeprecated>)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(tora::warning.deprecated.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wdeprecated>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(tora::warning.deprecated.cxx INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-Wdeprecated>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "NVHPC")
  target_compile_options(tora::warning.deprecated.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wdeprecated-declarations>)
endif()
