#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::coverage.c INTERFACE IMPORTED)
add_library(tora::coverage.cxx INTERFACE IMPORTED)

target_compile_options(tora::coverage.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fprofile-instr-generate>)
target_compile_options(tora::coverage.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fcoverage-mapping>)
target_link_options(tora::coverage.c INTERFACE -fprofile-instr-generate)
target_link_options(tora::coverage.c INTERFACE -fcoverage-mapping)


target_compile_options(
  tora::coverage.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fprofile-instr-generate>)
target_compile_options(
  tora::coverage.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fcoverage-mapping>)
target_link_options(
  tora::coverage.cxx INTERFACE -fprofile-instr-generate)
target_link_options(
  tora::coverage.cxx INTERFACE -fcoverage-mapping)

find_program(TORA_LLVM_PROFDATA llvm-profdata REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_BINARY_PATH_ARRAY} DOC "llvm-profdata executable")

find_program(TORA_LLVM_COV llvm-cov REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_BINARY_PATH_ARRAY} DOC "llvm-cov executable")

function(tora_coverage_process_group GROUP_NAME)
  if(NOT TARGET tora.coverage.${GROUP_NAME})
    message(FATAL_ERROR "Unknown Test Coverage Group: ${GROUP_NAME}")
  endif()

  set(PROFILE_FILE_ARRAY)
  foreach(TEST ${TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY})
    list(APPEND PROFILE_FILE_ARRAY
      "${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}/prof/${TEST}.prof")
  endforeach()

  set(TARGET_PATH_ARRAY)
  foreach(TARGET ${TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY})
    list(APPEND TARGET_PATH_ARRAY "-object")
    list(APPEND TARGET_PATH_ARRAY "$<TARGET_FILE:${TARGET}>")

  endforeach()

  add_custom_command(
    OUTPUT ${GROUP_NAME}.profdata
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND ${TORA_LLVM_PROFDATA} merge -sparse ${PROFILE_FILE_ARRAY} -o
    ${GROUP_NAME}.profdata
    DEPENDS ${TARGET_NAME})

  add_custom_command(
    OUTPUT ${GROUP_NAME}.dat
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${TORA_LLVM_COV} show ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt
    -show-instantiation-summary > ${GROUP_NAME}.dat
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_command(
    OUTPUT ${GROUP_NAME}.json
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${TORA_LLVM_COV} export ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt >
    ${GROUP_NAME}.json
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_command(
    OUTPUT ${GROUP_NAME}.html
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${TORA_LLVM_COV} show ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt --format=html
    -region-coverage-lt=100 > ${GROUP_NAME}.html
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_target(
    tora.coverage.${GROUP_NAME}.files
    DEPENDS ${GROUP_NAME}.dat ${GROUP_NAME}.html ${GROUP_NAME}.json
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR})

  add_dependencies(tora.coverage.${GROUP_NAME}
    tora.coverage.${GROUP_NAME}.files)
endfunction()

function(tora_coverage_initialize_group GROUP_NAME)
  add_custom_target(tora.coverage.${GROUP_NAME})
  add_dependencies(tora.coverage tora.coverage.${GROUP_NAME})

  set(TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL
    ${TORA_COVERAGE_OUTDIR}/${GROUP_NAME})

  set(TORA_COVERAGE_${GROUP_NAME}_OUTDIR
    ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}
    CACHE INTERNAL "${GROUP_NAME} Output Directory")

  set(TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY CACHE INTERNAL
    "${GROUP_NAME} Test Array")

  set(TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    CACHE INTERNAL "${GROUP_NAME} Target Array")

  file(MAKE_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/prof)

  file(WRITE ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/compiler "Clang\n")

  cmake_language(
    EVAL CODE "
    cmake_language(DEFER CALL tora_coverage_process_group [[${GROUP_NAME}]])
    ")
  endfunction()

  function(tora_coverage_register_test GROUP_NAME TEST_NAME TARGET_NAME)
    if(NOT TARGET tora.coverage.${GROUP_NAME})
      tora_coverage_initialize_group(${GROUP_NAME})
    endif()

    set_tests_properties(
      ${TEST_NAME}
      PROPERTIES
      ENVIRONMENT
      LLVM_PROFILE_FILE=${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}/prof/${TEST_NAME}.prof
      )

    list(APPEND TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY "${TEST_NAME}")

    set(TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY
      ${TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY}
      CACHE INTERNAL "${GROUP_NAME} Test Array")

    list(APPEND TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY "${TARGET_NAME}")

    set(TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
      ${TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY}
      CACHE INTERNAL "${GROUP_NAME} Test Array")
  endfunction()
