#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

include_guard(GLOBAL)

add_library(tora::coverage.c INTERFACE IMPORTED)
add_library(tora::coverage.cxx INTERFACE IMPORTED)

target_compile_options(tora::coverage.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fprofile-arcs>)
target_compile_options(tora::coverage.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-ftest-coverage>)
target_compile_options(tora::coverage.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fprofile-update=atomic>)
target_link_options(tora::coverage.c INTERFACE --coverage)

target_compile_options(tora::coverage.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fprofile-arcs>)
target_compile_options(tora::coverage.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-ftest-coverage>)
target_compile_options(tora::coverage.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fprofile-update=atomic>)
target_link_options(tora::coverage.cxx INTERFACE --coverage)

find_program(TORA_LCOV lcov REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_BINARY_PATH_ARRAY} DOC "lcov executable")

find_program(TORA_GENHTML genhtml REQUIRED NO_DEFAULT_PATH
  PATHS ${TORA_BINARY_PATH_ARRAY} DOC "genhtml executable")

function(tora_coverage_process_group GROUP_NAME)
  if(NOT TARGET tora.coverage.${GROUP_NAME})
    message(FATAL_ERROR "Unknown Test Coverage Group: ${GROUP_NAME}")
  endif()

  add_custom_command(
    OUTPUT ${GROUP_NAME}.info
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${CMAKE_COMMAND} -DCOVERAGE_OUTDIR=${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}
    -DGROUP_NAME=${GROUP_NAME} -DTORA_LCOV=${TORA_LCOV}
    -DTORA_GENHTML=${TORA_GENHTML} -P
    ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/gcc.impl.cmake)

  add_custom_target(tora.coverage.${GROUP_NAME}.files DEPENDS ${GROUP_NAME}.info
    WORKING_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR})

  add_dependencies(tora.coverage.${GROUP_NAME}
    tora.coverage.${GROUP_NAME}.files)
endfunction()

function(tora_coverage_initialize_group GROUP_NAME)
  add_custom_target(tora.coverage.${GROUP_NAME})
  add_dependencies(tora.coverage tora.coverage.${GROUP_NAME})

  set(TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL
    ${TORA_COVERAGE_OUTDIR}/${GROUP_NAME})

  set(TORA_COVERAGE_${GROUP_NAME}_OUTDIR
    ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}
    CACHE INTERNAL "${GROUP_NAME} Output Directory")

  set(TORA_COVERAGE_${GROUP_NAME}_GCDA_DIR
    ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR}/gcda/
    CACHE INTERNAL "${GROUP_NAME} GCDA Output Directory")

  set(TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY CACHE INTERNAL
    "${GROUP_NAME} Test Array")

  set(TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    CACHE INTERNAL "${GROUP_NAME} Target Array")

  file(MAKE_DIRECTORY ${TORA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/prof)

  cmake_language(
    EVAL CODE "
    cmake_language(DEFER CALL tora_coverage_process_group [[${GROUP_NAME}]])
    ")
  endfunction()

  function(tora_coverage_register_test GROUP_NAME TEST_NAME TARGET_NAME)
    if(NOT TARGET tora.coverage.${GROUP_NAME})
      tora_coverage_initialize_group(${GROUP_NAME})
    endif()

    set_tests_properties(
      ${TEST_NAME} PROPERTIES ENVIRONMENT
      GCOV_PREFIX=${TORA_COVERAGE_${GROUP_NAME}_GCDA_DIR})

    list(APPEND TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY "${TEST_NAME}")

    set(TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY
      ${TORA_COVERAGE_${GROUP_NAME}_TEST_ARRAY}
      CACHE INTERNAL "${GROUP_NAME} Test Array")

    list(APPEND TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY "${TARGET_NAME}")

    set(TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
      ${TORA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY}
      CACHE INTERNAL "${GROUP_NAME} Test Array")
  endfunction()
