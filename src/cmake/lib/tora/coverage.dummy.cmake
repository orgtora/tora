#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

include_guard(GLOBAL)
message(DEBUG "Enabling tora::coverage.dummy")

set(TORA_COVERAGE_OUTDIR ${PROJECT_BINARY_DIR}/tora/coverage
    CACHE PATH "Path to Store Output of Coverage Data")

include(${CMAKE_CURRENT_LIST_DIR}/coverage/dummy.cmake)
message(DEBUG "Enabling tora::coverage.dummy - Done")
