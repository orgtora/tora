#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(tora::instrument.asan.c INTERFACE IMPORTED)
add_library(tora::instrument.asan.cxx INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize=address>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=address>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize-address-use-after-scope>)
  target_link_options(tora::instrument.asan.c INTERFACE    -fsanitize=address)
  target_link_options(tora::instrument.asan.c INTERFACE    -fno-sanitize-recover=address)
  target_link_options(tora::instrument.asan.c INTERFACE    -fno-omit-frame-pointer)
  target_link_options(tora::instrument.asan.c INTERFACE    -fno-optimize-sibling-calls)
  target_link_options(tora::instrument.asan.c INTERFACE    -fsanitize-address-use-after-scope)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GCC")
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize=address>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=address>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>)
  target_compile_options(
    tora::instrument.asan.c INTERFACE $<$<COMPILE_LANGUAGE:C>:-fsanitize-address-use-after-scope>)

  target_link_options(tora::instrument.asan.c INTERFACE -fsanitize=address)
  target_link_options(tora::instrument.asan.c INTERFACE -fno-sanitize-recover=address)
  target_link_options(tora::instrument.asan.c INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.asan.c INTERFACE -fno-optimize-sibling-calls)
  target_link_options(tora::instrument.asan.c INTERFACE -fsanitize-address-use-after-scope)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=address>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=address>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fsanitize-address-use-after-scope>)


  target_link_options(tora::instrument.asan.cxx INTERFACE -fsanitize=address)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-sanitize-recover=address)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-optimize-sibling-calls)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fsanitize-address-use-after-scope)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GCC")
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=address>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=address>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>)
  target_compile_options(
    tora::instrument.asan.cxx INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-fsanitize-address-use-after-scope>)

  target_link_options(tora::instrument.asan.cxx INTERFACE -fsanitize=address)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-sanitize-recover=address)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-omit-frame-pointer)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fno-optimize-sibling-calls)
  target_link_options(tora::instrument.asan.cxx INTERFACE -fsanitize-address-use-after-scope)
endif()
